from django.urls import path
from . import views
from django.views.generic import ListView, DetailView
from .models import Blog
from django.contrib.auth import views as auth_views # Gọi views của django


urlpatterns = [
    path('', views.IndexListView, name='index'),
    path('<int:pk>/', views.ViewDetail, name='detailview'),
    path('logout/', auth_views.LogoutView.as_view(next_page="/blog"), name='logout'),
    path('register/', views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name="home/login.html"),name='login'),
    path('contact/',views.contact,name='contact'),
    path('otherInformation/',views.otherInformation,name='otherInformation'),
    path('search/',views.search,name='search'),
    path('category/<int:id>/', views.ViewCategory, name='ViewCategory'),
]
