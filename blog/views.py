from django.shortcuts import render,get_object_or_404,redirect
from django.views.generic import ListView,DetailView
from .models import Blog,Comment,Category
from .Form import CommentForm,RegistrationFrom
from django.http import HttpResponseRedirect
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator


# Create your views here.

def ViewDetail(request, pk):
    post = Blog.objects.get(pk=pk)
    form = CommentForm()
    if request.method == 'POST':
        form = CommentForm(request.POST,author=request.user,post=post)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(request.path)
    return render(request, 'home/detailblog.html', {'post':post, 'form':form,})

def ViewCategory(request,id):
    get_post_categorys = Blog.objects.filter(cat_id=id)
    Categorys = Category.objects.all()
    return render(request, 'home/categoryview.html', {'get_post_categorys':get_post_categorys, 'Categorys':Categorys})

def IndexListView(request):
    Blogs = Blog.objects.all().order_by("-date") 
    Categorys = Category.objects.all()
    getitem = Blog.objects.filter()[:5] 
    paginator = Paginator(Blogs, 2) # Show 25 contacts per page

    page = request.GET.get('page')
    contacts = paginator.get_page(page)

    return render(request, 'home/index.html',{'Blogs':Blogs, 'Categorys':Categorys, 'ViewDetail_page': 'active', 'contacts': contacts})

# class IndexListView(ListView):
#     queryset = Blog.objects.all().order_by("-date") 
#     template_name = "home/index.html"
#     context_object_name = "Blogs"
#     paginate_by = 5

# class ViewDetail(DetailView):
#     model = Blog
#     template_name = "home/detailblog.html"


def register(request):
    form = RegistrationFrom()
    if request.method == 'POST':
        form = RegistrationFrom(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/blog')
    return render(request,'home/register.html',{'form':form})

def contact(request):
    context = {"contact_page": "active"}
    return render(request,'home/contact.html',context)

def otherInformation(request):
    context = {"otherInformation_page": "active"}
    return render(request,'home/otherInformation.html',context)

def search(request):        
    if request.method == 'GET': # this will be GET now      
        blog_name =  request.GET.get('search') # do some research what it does       
        status = Blog.objects.filter(title=blog_name) # filter returns a list so you might consider skip except part
        return render(request,"home/search.html", {"blogs": status})
    else:
        return render(request,"home/search.html",{})

def send_email(request):
    subject = request.POST.get('subject', '')
    message = request.POST.get('message', '')
    from_email = request.POST.get('from_email', '')
    if subject and message and from_email:
        try:
            send_mail(subject, message, from_email, ['admin@example.com'])
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponseRedirect('/blog/contact/')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Make sure all fields are entered and valid.')
