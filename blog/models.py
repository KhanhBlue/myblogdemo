from django.db import models
from django.conf import settings
from tinymce import HTMLField

# Create your models here.
class Category(models.Model):
    cat_url = models.CharField(max_length=100, null=True)
    cat_id = models.IntegerField()
    cate_parent_id = models.IntegerField()
    cate_name = models.CharField(max_length=200)
    description = HTMLField()
    cat_status = models.BooleanField(default=True) 

    def __str__(self):
        return self.cate_name

class Blog(models.Model):
    blog_url = models.CharField(max_length=200, null=True)
    title = models.CharField('Tiêu đề',max_length=100)
    body = HTMLField("Nội dung")
    date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField()
    cat_id = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='categorys',null=True)

    def __str__(self):
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Blog,on_delete=models.CASCADE,related_name='comments')
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
