from django.contrib import admin
from blog.models import *
from django.contrib.auth.models import Group

# Register your models here.

admin.site.site_header = 'Manager my blog'
admin.site.unregister(Group)

class CommentInline(admin.StackedInline):
    model = Comment

class AuthorAdmin(admin.ModelAdmin):
    inlines = [CommentInline]
    list_display = ('title', 'date')
    list_filter = ['date']
    search_fields = ['title']
    

admin.site.register(Blog,AuthorAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('cate_name', 'description','cate_parent_id')
    search_fields = ['cate_name']

admin.site.register(Category,CategoryAdmin)